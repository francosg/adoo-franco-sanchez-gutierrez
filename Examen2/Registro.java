public class Registro {
    private Alumno alumno;

    public Registro(Alumno alumno) {
        this.alumno = alumno;
    }

    public void notificar() {
        System.out.println("Notificando al alumno: " + alumno.getNombre());
        // Lógica de notificación por correo electrónico, mensaje, etc.
    }
}