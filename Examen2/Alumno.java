public class Alumno {
    private String nombre;
    private int generacion;
    private String institucion;
    private String carrera;
    private boolean activo;

    public Alumno(String nombre, int generacion, String institucion, String carrera, boolean activo) {
        this.nombre = nombre;
        this.generacion = generacion;
        this.institucion = institucion;
        this.carrera = carrera;
        this.activo = activo;
    }


    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getGeneracion() {
        return generacion;
    }

    public void setGeneracion(int generacion) {
        this.generacion = generacion;
    }

    public String getInstitucion() {
        return institucion;
    }

    public void setInstitucion(String institucion) {
        this.institucion = institucion;
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}