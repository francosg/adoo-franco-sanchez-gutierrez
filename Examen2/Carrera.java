import java.util.List;
import java.util.ArrayList;

public class Carrera {
    private List<String> carreras;

    public Carrera() {
        this.carreras = new ArrayList<>();
    }

    public List<String> getCarreras() {
        return carreras;
    }

    public void setCarreras(List<String> carreras) {
        this.carreras = carreras;
    }
}