import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Sistema {
    private Date fechaLimite;
    private List<Alumno> alumnos;

    public Sistema(Date fechaLimite) {
        this.fechaLimite = fechaLimite;
        this.alumnos = new ArrayList<>();
    }

    public void inscribirAlumno(Alumno alumno) {
        alumnos.add(alumno);
    }

    public void generarReporte() {
        System.out.println("Reporte de alumnos:");
        for (Alumno alumno : alumnos) {
            System.out.println("-Nombre: " + alumno.getNombre());
            System.out.println("-Generación: " + alumno.getGeneracion());
            System.out.println("-Institución: " + alumno.getInstitucion());
            System.out.println("-Carrera: " + alumno.getCarrera());
            System.out.println("-Activo: " + alumno.isActivo());
            System.out.println();
        }
    }

    public void cambiarFechaLimite(Date nuevaFechaLimite) {
        this.fechaLimite = nuevaFechaLimite;
        System.out.println("Se ha cambiado la fecha límite a: " + nuevaFechaLimite);
    }

}