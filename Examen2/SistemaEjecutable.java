import java.util.Date;


public class SistemaEjecutable {
    public static void main(String[] args) {
        Sistema sistema = new Sistema(new Date());

        Alumno alumno1 = new Alumno("Luis", 2020, "Universidad Autonoma de Zacatecas", "Ingenieria en computacion", true);
        Alumno alumno2 = new Alumno("Bianca", 2019, "Instituto Politecnico Nacional", "Ingenieria en alimentos", true);
        Alumno alumno3 = new Alumno("Juan", 2017, "Universidad Autonoma de Zacatecas", "Ingenieria electrica", false);

        sistema.inscribirAlumno(alumno1);
        sistema.inscribirAlumno(alumno2);
        sistema.inscribirAlumno(alumno3);

        sistema.generarReporte();

        Registro registro = new Registro(alumno1);
        registro.notificar();

        Registro registro2 = new Registro(alumno2);
        registro2.notificar();


        Registro registro3 = new Registro(alumno3);
        registro3.notificar();

        Carrera carrera = new Carrera();
        carrera.getCarreras().add("Ingenieria electrica");
        carrera.getCarreras().add("Ingenieria civil");
        carrera.getCarreras().add("Ingenieria en computacion");
        carrera.getCarreras().add("Ingenieria en alimentos");
        carrera.getCarreras().add("Ingenieria mecatronica");


        System.out.println("Carreras disponibles: " + carrera.getCarreras());

        Date nuevaFechaLimite = new Date(); //Metodo para aniadir una nueva fecha limite
        sistema.cambiarFechaLimite(nuevaFechaLimite);

    }
}
